# Remion recruitment test Java template #

### Requirements
* JDK 8  
* [Maven wrapper](https://github.com/takari/maven-wrapper) (Linux/iOS: `mvnw`, Windows `mvnw.cmd`) (Included)
* [Maven](https://maven.apache.org/)  (Optional) 

### Package
`mvnw package`

### Run
`mvnw exec:java -Dexec.mainClass=com.remion.Sample -Dexec.args="1 2 3"`